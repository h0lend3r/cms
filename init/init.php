<?php

session_start();

include_once "helpers/functions.php";

spl_autoload_register(function($class) {
	include_once "libs/{$class}.php";
});

$initCMS = new Bootstrap();

if(!Cookie::has('language'))
{
	Cookie::set('language', 'en', time() + (60*60*24*365*15), "/");
}