(function($) {
"use strict";

	$.fn.changeLanguage = function()
	{
			var $select   = $(this),
				$type 	  = $select.parent().attr('method'),
				$url	  = $select.parent().attr('action'),
				$data     = {};

			$select.children().filter(function() {
			    return $(this).val() == $.cookie('language'); 
			}).prop('selected', true);

			$select.change(function()
			{

				$data['language'] = $(this).val();

				$.ajax(
				{
					url: $url,
					type: $type,
					data: $data,
					success: function()
				    {
				        location.reload(true);
					}
				});

			});
			
	}

}( jQuery ));