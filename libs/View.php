<?php

class View
{

	public static function make($path)
	{
		try
		{
			self::_makeView($path);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	private static function _makeView($path)
	{
		

		$filename = explode('/', $path);
		$filename = end($filename);

		$language = Cookie::get("language");

		$file = "views/" . $language . "/{$path}.php";

		if(file_exists($file))
		{
			include_once $file;
		}
		else
		{
			throw new Exception("View: <b>{$filename}</b> could not be found.");
		}
	}

	public static function template($name, $extension = null)
	{
		$language = Cookie::get('language');

		if(isset($extension))
		{
			$file = "views/template/{$language}/{$name}.{$extension}";
			include_once $file;
		}
		else
		{
			$file = "views/template/{$language}/{$name}.php";
			include_once $file;
		}
	}

}