<?php

class Cookie
{

	public static function has($name)
	{
		return isset($_COOKIE[$name]) ? true : false;
	}

	public static function get($name)
	{
		return isset($_COOKIE[$name]) ? $_COOKIE[$name] : "This cookie doesn't exists.";
	}

	public static function set($name, $value, $time = null, $path = null)
	{
		if(isset($time))
		{
			if(isset($path))
			{
				return setcookie($name, $value, $time, $path);
			}
			return setcookie($name, $value, $time);
		}
		else
		{
			return setcookie($name, $value);
		}
	}

}