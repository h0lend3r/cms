<?php

class Bootstrap
{

	private $controllerExtension = "Controller";

	private $controllerName 	 = "Index";

	private $methodName 		 = "index";

	private $params 			 = [];

	private $controller;

	public function __construct()
	{
		try
		{
			$this->_runController();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}

	}

	private function _parseURL()
	{
		if(isset($_GET['url']))
		{
			return $url = explode("/", filter_var(rtrim($_GET['url'], "/"), FILTER_SANITIZE_URL));
		}
	}

	private function _runController()
	{
		$url = $this->_parseURL();


		if(isset($url[0]))
		{
			$this->controllerName = $url[0];
		}

		$controllerPath = "controllers/" . $this->controllerName . $this->controllerExtension . '.php';

		if(file_exists($controllerPath))
		{
			include_once $controllerPath;

			$controllerName = $this->controllerName . $this->controllerExtension;

			$this->controller = new $controllerName;

			if(isset($url[1]))
			{
				$this->methodName = $url[1];
				unset($url[1]);
			}

			$this->params = $url ? array_values($url) : [];

			try
			{
				$this->_runMethod();
			}
			catch(Exception $e)
			{
				echo $e->getMessage();
			}
		}
		else
		{
			throw new Exception("Controller: <b>{$this->controllerName}</b> has not been found.");
		}
	}

	private function _runMethod()
	{


		if(isset($url[1]))
		{
			$this->methodName = $url[1];
		}

		if(method_exists($this->controller, $this->methodName))
		{
			call_user_func_array([$this->controller, $this->methodName], $this->params);
		}
		else
		{
			throw new Exception("Mehthod: <b>{$this->methodName}</b> has not been found.");
		}
	}


	// Setters for Bootstrap's attributes
}