<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="<?= css("bootstrap") ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
		  	<div class="container-fluid">
		    	<!-- Brand and toggle get grouped for better mobile display -->
		    	<div class="navbar-header">
		    	  	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        		<span class="sr-only">Toggle navigation</span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		        		<span class="icon-bar"></span>
		      		</button>
		      		<a class="navbar-brand" href="#">PL VERSION</a>
		    	</div>
					
				<!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			    	
			      	<form method="get" action="language/change" class="navbar-form navbar-right" >
						<select name="language" id="language" class="form-control">
							<option value="en">English</option>
							<option value="pl">Polski</option>
						</select>
					</form>

		    	</div><!-- /.navbar-collapse -->
		  	</div><!-- /.container-fluid -->
		</div><!-- /.container -->
	</nav>

	<div class="container">